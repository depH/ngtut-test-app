'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
    'ngRoute',
    'phonecatControllers',
    'ProductService'
]);

phonecatApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'tpl/home.html'
        });

        $routeProvider.when('/products', {
            templateUrl: 'tpl/products.html',
            controller: 'ProductList'
        });

        $routeProvider.
            when('/phones', {
                templateUrl: 'tpl/phone-list.html',
                controller: 'PhoneListCtrl'
            }).
            when('/phones/:phoneId', {
                templateUrl: 'tpl/phone-detail.html',
                controller: 'PhoneDetailCtrl'
            }).
            otherwise({
                redirectTo: '/home'
            });
    }]);