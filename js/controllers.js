var phonecatControllers = angular.module('phonecatControllers', ['snap']);

phonecatControllers.controller('PhoneListCtrl', ['$scope', '$http',
    function($scope, $http) {
        $http.get('datastore/phones.json').success(function(data) {
            $scope.phones = data;
        });
        $scope.orderProp = 'age';
    }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams',
    function($scope, $routeParams) {
        $scope.phoneId = $routeParams.phoneId;
    }]);

phonecatControllers.controller('NavCtrl', ['$scope', '$routeParams',
    function($scope, $routeParams) {
        $scope.Home = function () {
            window.location = "#/home";
        };

        $scope.PhoneList = function () {
            window.location = "#/phones";
        };
        $scope.ProductList = function () {
            window.location = "#/products";
        };


        $scope.phoneId = $routeParams.phoneId;
    }]);

phonecatControllers.controller('ProductList', ['$scope', 'Products',
      function($scope, Products) {
            $scope.productslist = Products.query();
            $scope.orderProp = 'age';
    }]);


phonecatControllers.controller('MainCtrl', function ($scope) {
        $scope.snapOpts = {
            disable: 'right'
        };
    });