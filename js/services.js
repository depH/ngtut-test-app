var ProductService = angular.module('ProductService', ['ngResource']);

ProductService.factory('Products', ['$resource',
    function($resource){
        var res =  $resource('http://54.253.122.4/~wwwsortedstorage/test.php', {}, {
            query: {
                method:'POST',
                params:{count:3},
                isArray:true,
                interceptor : {
                    responseError : function(errorObj){
                        /* console.log("In Error method of responseError")*/
                    },
                    response    : function(responseObj){
                        /* console.log("In  method of response");*/
                    }
                }
            }
        });
        return res;
    }]);